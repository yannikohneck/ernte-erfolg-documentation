# Umgebungen
## Entwicklung
* https://dev.ernte-erfolg.de
* https://dev.ernte-erfolg.de/api

## Preproduktion
* https://testing.ernte-erfolg.de
* https://testing.ernte-erfolg.de/api

## Produktion
* https://ernte-erfolg.de
* https://ernte-erfolg.de/api

# Server 
## Deployment starten 
Auf dem Server:
```
cd /opt/ernte-erfolg/ernte-erfolg-deploy  
./deployer up development
./deployer up production
```

### CI/CD - Deployment / Release Flow
Main/Default Branch von ernte-erfolgfrontend und ernte-erfolg-backend ist master.

Entwickelt wird in Feature Branches, die Namen dieser Feature Branches beginnen mit "/feature/" gefolgt von der Jira Issue ID und optionalem Titel

- /feature/ER-122
- /feature/ER-123_Behebe_den_Bug

#### Issue Workflow
1. Ist die Entwicklung eines Issues (Dev_Task) abgeschlossen, so wird ein MergeRequest erstellt, der dann von einem weiteren Entwickler "gereviewed" und freigegeben wird.  
2. Am besten den Merge Request den potentiellen "Reviewern" assignen (gerne mehrere!). 
3. Das Jira Issue zum MergeRequest ist an den Reviewer oder an "unassigned" zu assignen. Und mir einem Kommentar versehen: "Bitte um Review" mit Link zu dem MergeRequest.
4. Das Issue wird in die Sprint-Board Spalte "ReadyToTest" geschoben. 
5. Beim Abarbeiten von Issies gilt: "Von Rechts nach Links" ! D.h. bevor ein neues Issue angefangen wird, sind zuerst die Reviews zu erledigen. Jeder sollte in der Lage sein ein Dev Issue zu reviewen und den Merge Request in Gitlab zu approven.
6. Die approved MergeRequests werden dann von einem Maintainer auf master gemerged. Dazu bitte das Issue mit einem Kommentar einem Maintainer zuweisen!

Jeder commit auf den master Branch triggert ein automatisched Deployment auf die dev Umgebung!

Sobald eine stabile Version für die Testumgebung auf master verfügbar ist, wird diese getagged. Das taggen triggert einen job, der automatisch die getaggte Version auf die testing Umgebung deployed.
Daher dürfen nur Maintainer taggen.

Ausserdem wird ein manuell zu triggernder Job erzeugt, der die gettagte Version auf die prod Umgebung deployed. Diese kann angestoßen werden, sobald die Version die auf testing getestet wurede, von testManagement freigegeben wird.

#### Tagging 
Als Release Candidate (zu Deutsch "Freigabekandidat") bezeichnet man eine Vorabversion, die bereits alle Funktionen des endgültigen Produktes enthält.
Nach der Veröffentlichung eines Release Canidates ist in der Regel die eigentliche Entwicklungsarbeit abgeschlossen.
Ab diesem Zeitpunkt konzentrieren sich die Entwickler im Allgemeinen auf die Beseitigung von Fehlern und verpassen ihrem Produkt den notwendigen Feinschliff.
Nachdem wir einen abgenommenen RELEASE_CANDIDATE haben, wird dieser zusätzlich mit dem Tag "STABLE_RELEASE_x.y" (aktuell: STABLE_RELEASE_1.0) gettaged. Diese Version geht dann in Produktion.
Tag Namen "RELEASE_CANDIDATE_x.y" und "STABLE_RELEASE_x.y" versuchte ich so zu wählen, dass wir sie mit wildcards in gitlab-ci unterscheiden können, somit können wir die jobs enablen/disablen. (So der Plan) (bearbeitet) 

##### Update 5.5.2020

1.  erne-erfolg-frontend und erne-erfolg-backend ist nun so konfiguriert, das jeder Tags erstellen kann. 
2.  Tags RELEASE_CANDIDATE_* und STABLE_RELEASE*  können nur von Maintainern erzeugt werden, da diese die testing / produktion deployments anstoßen.


| Job Name | erzeugt bei | trigger | deployed auf |
| ---      |  ------  |  ------  |---------:|
| deploy-development | push auf master | automatisch | development |
| deploy-development-tag   | erzeugung eines tags | manuell | development |
| deploy-testing  | Tagging mit RELEASE_CANDIDATE*  | automatisch | testing  |
| deploy-prod   | Tagging mit STABLE_RELEASE*  | manuell   | produktion   |

##### Update 10.03.2020 Jira Releases
Die Jira Releases orientieren sich auch an den Tags RELEASE_CANDIDATE_* und STABLE_RELEASE*.
Allerdings wird in Jira Releases bei RELEASE_CANDIDATE_* "BE_" (Backend) bzw. "FE_" (Frontend) als Prefix verwendet. 
Bei STABLE_RELEASE* verzichen wir vorerst auf diese Prefix, das kann sich aber ändern !

# Backend

## Konfiguration
In [docker-compose.yml](https://gitlab.com/ernte-erfolg/ernte-erfolg-deploy/-/blob/master/docker-compose.yml) werden Environment Variablen gesetzt(überschrieben),
die in [.env.development](https://gitlab.com/ernte-erfolg/ernte-erfolg-deploy/-/blob/master/.env.development) bzw. [.env.production](https://gitlab.com/ernte-erfolg/ernte-erfolg-deploy/-/blob/master/.env.production) gesetzt werden.

## Logging
`docker logs ee_development_backend_1`

## Branches
### 16.04.2020:
Da wir ja auf master statt develop entwickeln wollen, werde ich den develop Branch nun löschen!
Ich habe davor nochmal develop auf master gemerged.
Für alle Fälle hab ich vor dem löschen nochmal getagged https://gitlab.com/ernte-erfolg/ernte-erfolg-backend/-/tags/tag_on_develop_before_deleting_branch (bearbeitet) 

Es gibt noch kein automatisches Deployment auf den Server: https://ernteerfolg.atlassian.net/browse/ER-78

Es gibt jetzt die Branches 
#### master (dafault), 
Allowed to push: NoOne, Allowed to merge: "Maintainers". 
Bei einem push wird ein Docker Image gebaut und mit dem Tag "development" in die Docker Registry hochgeladen.

Feature Branches werden von master erstellt und auf master zurück gemerged.

#### preproduction, 
Allowed to push: NoOne, Allowed to merge: "Kevin und Fred". 
Bei einem push wird ein Docker Image gebaut und mit dem Tag "preproduction" in die Docker Registry hochgeladen.

#### production, 
Allowed to push: NoOne, Allowed to merge: "Kevin und Fred". 
Bei einem push wird ein Docker Image gebaut und mit dem Tag "production" in die Docker Registry hochgeladen.
