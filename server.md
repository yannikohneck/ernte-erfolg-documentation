# Overview
* Hetzner CX21: 2 Cores, 4GB RAM, 40GB Disk
* IP Adress: 2a01:4f8:c17:86ea::1 (legacy: 49.12.43.0)
* FQDN: hopper.ernte-erfolg.de
* OS: Debian 10
* Domains: ernte-erfolg.de, www.ernte-erfolg.de, api.ernte-erfolg.de

# Basic System
* Installed packages:
  - postfix
  - bsd-mail
  - libsasl2-modules
  - ngninx
  - ufw
  - certbot
  - python-certbot-nginx
  - docker
  - docker-compose
  - apticron
  - logwatch

## Postfix config
```
smtpd_relay_restrictions = permit_mynetworks permit_sasl_authenticated defer_unauth_destination
myhostname = hopper
alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
myorigin = /etc/mailname
mydestination = hopper.ernte-erfolg.de, hopper, localhost.localdomain, localhost
relayhost = betelgeuse.uberspace.de:587
mynetworks = 127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128 172.16.42.0/24
mailbox_size_limit = 0
recipient_delimiter = +
inet_interfaces = 127.0.0.1, 172.16.42.1, [::1]
inet_protocols = all
myorigin = /etc/mailname

smtp_sasl_auth_enable = yes
smtp_sasl_security_options = noanonymous
smtp_sasl_password_maps = hash:/etc/postfix/sasl_password
smtp_tls_security_level = encrypt
```

## ufw config
```
Status: active
Logging: on (low)
Default: deny (incoming), allow (outgoing), deny (routed)
New profiles: skip

To                         Action      From
--                         ------      ----
22/tcp                     ALLOW IN    Anywhere                  
80/tcp                     ALLOW IN    Anywhere                  
443/tcp                    ALLOW IN    Anywhere                  
172.16.42.1 25/tcp         ALLOW IN    172.16.42.0/24            
7890/tcp                   ALLOW IN    Anywhere                  
22/tcp (v6)                ALLOW IN    Anywhere (v6)             
80/tcp (v6)                ALLOW IN    Anywhere (v6)             
443/tcp (v6)               ALLOW IN    Anywhere (v6)             
7890/tcp (v6)              ALLOW IN    Anywhere (v6)   
```

## nginx config
### ernte-erfolg.de
```
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        server_name ernte-erfolg.de www.ernte-erfolg.de;
        return 301 https://$server_name$request_uri;
}

server {
        listen 443 ssl http2 default_server;
        listen [::]:443 ssl http2 default_server;
        server_name ernte-erfolg.de www.ernte-erfolg.de;

        ssl_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/ernte-erfolg.de/privkey.pem;
        ssl_protocols TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256;
        # Load DH parameters
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        ssl_ecdh_curve secp521r1:secp384r1:prime256v1;
        # Shared cache size 30MB
        ssl_session_cache shared:SSL:30m;
        # Default timeout is 5m
        ssl_session_timeout 10m;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Frame-Options DENY;
        add_header Referrer-Policy "same-origin";
        add_header Expect-CT "enforce, max-age=21600";
        add_header Content-Security-Policy "default-src 'none'; img-src 'self' https://ernte-erfolg.de; style-src 'self' 'unsafe-inline'; font-src 'self'; base-uri 'none';frame-ancestors 'none'; form-action 'self'; block-all-mixed-content";

        ssl_stapling on;
        ssl_trusted_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_stapling_verify on;

        access_log /var/log/nginx/ernte-erfolg.de_access.log;
        error_log /var/log/nginx/ernte-erfolg.de_error.log;

        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_buffers 16 4k;
                proxy_buffer_size 2k;
                proxy_pass http://172.16.42.2:8080;
        }
}
```

### api.ernte-erfolg.de
```
server {
        listen 80;
        listen [::]:80;
        server_name api.ernte-erfolg.de;
        return 301 https://$server_name$request_uri;
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name api.ernte-erfolg.de;

        ssl_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/ernte-erfolg.de/privkey.pem;
        ssl_protocols TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256;
        # Load DH parameters
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        ssl_ecdh_curve secp521r1:secp384r1:prime256v1;
        # Shared cache size 30MB
        ssl_session_cache shared:SSL:30m;
        # Default timeout is 5m
        ssl_session_timeout 10m;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Frame-Options DENY;
        add_header Referrer-Policy "same-origin";
        add_header Expect-CT "enforce, max-age=21600";

        ssl_stapling on;
        ssl_trusted_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_stapling_verify on;

        access_log /var/log/nginx/api.ernte-erfolg.de_access.log;
        error_log /var/log/nginx/api.ernte-erfolg.de_error.log;

        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_buffers 16 4k;
                proxy_buffer_size 2k;
                proxy_pass http://172.16.42.3:8080;
        }
}
```

### hopper.ernte-erfolg.de
```
server {
        listen 80;
        listen [::]:80;
        server_name hopper.ernte-erfolg.de;
        return 301 https://$server_name$request_uri;
}

server {
        listen 443 ssl http2;
        listen [::]:443 ssl http2;
        server_name hopper.ernte-erfolg.de;

        ssl_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/ernte-erfolg.de/privkey.pem;
        ssl_protocols TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256;
        # Load DH parameters
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        ssl_ecdh_curve secp521r1:secp384r1:prime256v1;
        # Shared cache size 30MB
        ssl_session_cache shared:SSL:30m;
        # Default timeout is 5m
        ssl_session_timeout 10m;

        add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload";
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";
        add_header X-Frame-Options DENY;
        add_header Referrer-Policy "same-origin";
        add_header Expect-CT "enforce, max-age=21600";

        ssl_stapling on;
        ssl_trusted_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_stapling_verify on;

        access_log /var/log/nginx/hopper.ernte-erfolg.de_access.log;
        error_log /var/log/nginx/hopper.ernte-erfolg.de_error.log;

        auth_basic           "Administrator's Area";
        auth_basic_user_file /etc/apache2/.htpasswd;

        location /adminer {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_buffers 16 4k;
                proxy_buffer_size 2k;
                proxy_pass http://172.16.42.5:8080;
        }

        location / {
                root /var/www/html;
                index index.html;
                try_files $uri $uri/ =404;
        }

        location = / {
                root /var/www/html;
                index index.html;
                try_files $uri $uri/ =404;
        }
}

map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
}
 
upstream websocket {
        server 172.16.42.99:7890;
}

server {
        listen 7890 ssl;
        listen [::]:7890 ssl;
        server_name hopper.ernte-erfolg.de;

        ssl_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_certificate_key /etc/letsencrypt/live/ernte-erfolg.de/privkey.pem;
        ssl_protocols TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256;
        # Load DH parameters
        ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
        ssl_ecdh_curve secp521r1:secp384r1:prime256v1;
        # Shared cache size 30MB
        ssl_session_cache shared:SSL:30m;
        # Default timeout is 5m
        ssl_session_timeout 10m;

        ssl_stapling on;
        ssl_trusted_certificate /etc/letsencrypt/live/ernte-erfolg.de/fullchain.pem;
        ssl_stapling_verify on;

        access_log /var/log/nginx/hopper.ernte-erfolg.de_ws_access.log;
        error_log /var/log/nginx/hopper.ernte-erfolg.de_ws_error.log;
        
        location /ws {
            proxy_pass http://websocket;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
            proxy_set_header Host $host;
        }
}
```

## goaccess
```
docker run -d --restart always --name goaccess --network ernte-erfolg-deploy_app_net --ip 172.16.42.99 -e TZ="Europe/Berlin" -e LANG=$LANG -v "/var/www/html/index.html:/var/www/html/index.html" -v "/var/log/nginx:/var/log/nginx:ro" -v "/usr/share/GeoIP:/usr/share/GeoIP:ro" allinurl/goaccess /var/log/nginx/ernte-erfolg.de_access.log* -a -o /var/www/html/index.html --anonymize-ip --all-static-files --real-os --real-time-html --ws-url wss://hopper.ernte-erfolg.de:7890/ws --origin https://hopper.ernte-erfolg.de --log-format=COMBINED
```
