# Tools
Allgemein:
* [Trello Board](https://trello.com/b/OwFF6j2Y/ernte-erfolg)
* [Jira](https://ernteerfolg.atlassian.net/secure/RapidBoard.jspa?projectKey=ER&rapidView=1&atlOrigin=eyJpIjoiOTQwNTY4MTdiNWM2NGRmZDkxZjUzMThlYmEyOWZhYWIiLCJwIjoiaiJ9)
* [DEVPOST](https://devpost.com/software/plattform-fur-bauern-und-helfer)
* [Google Drive](https://drive.google.com/drive/folders/1_j5pYN2_BndBFvgqZHRAqE9k-MmC2SxK)
* [Google Calender](https://calendar.google.com/calendar/embed?src=vkg8nj9eoqtfrbiqvgf7dn9vrc%40group.calendar.google.com&ctz=Europe%2FBerlin) 
* [Analytics/Server statistics](https://analytics.ernte-erfolg.de/) 

Wireframes:
* [Ernteerfolg_SD_0.2 - latest](https://invis.io/KNWMLQOZRWJ)
* [Ernteerfolg_SD_0.1](https://invis.io/BDWIPJ39G4C)

Slack Channels:
* [ernte-erfolg](https://wirvsvirus.slack.com/archives/G010J7F52S2) --> externe Helfer und Unterstützer, Bekannte und Interessierte
* [ernte-erfolg-rundum](https://wirvsvirus.slack.com/archives/G0111741SP8) --> News zum Thema Erntehilfe, Saisonarbeitskräfte
* [ernte-erfolg-de-intern](https://wirvsvirus.slack.com/archives/G010R79J856) --> Kernteam, sensible Infos und Dinge, die für das gesamte interne Team interessant sind
* [ernte-erfolg-dev](https://wirvsvirus.slack.com/archives/G01058Y1U83) --> FrontEnd, BackEnd, Infra, Datenmodell, APIs
* [ernte-erfolg-dev-review](https://wirvsvirus.slack.com/archives/C01397MR0NM) --> Austausch mit 5minds IT-Solutions GmbH bzgl. Code-Review
* [ernte-erfolg-marketing](https://wirvsvirus.slack.com/archives/G010JTBKH38) --> Marketing, Social Media, PR, Kooperationen
* [ernte-erfolg-bölw-kooperation](https://wirvsvirus.slack.com/archives/G0116N33X1T) --> Abstimmung zur Kooperation mit BÖLW
* [ernte-erfolg-testmanagement](https://wirvsvirus.slack.com/archives/G0118LDRP38) --> Rund um das Management von Softwaretests
* [ernte-erfolg-rechtundfinanzen](https://wirvsvirus.slack.com/archives/G010YKD7473) --> Finanzen, Recht, Datenschutz, Organisations-/Unternehmensform

Social Media Kanäle:
* [Instagram](https://www.instagram.com/ernteerfolg/)
* [Facebook](https://www.facebook.com/ernteerfolg/)
* [Twitter](https://twitter.com/ErfolgErnte)

# Umgebungen
siehe [technischeDoku](technischeDoku.md) oder [Testmanagement](https://ernteerfolg.atlassian.net/wiki/spaces/EW/pages/163860/Testmanagement)

# Team
## 1) Product Owner
* Madgalena Kuhn - Data Scientist, Python, SQL, Docker, Airflow
## 2) Stabstelle Datenschutz
* Christian Paffhausen - Datenschutz
* Myriam Rapior - Datenschutz
## 3) Unternehmensentwicklung, Finanzen und Recht
* Arlette Beck - Unternehmensentwicklung und -strategie, Geschäftsidee, Business Plan
* Yannic - Teamsupport und Kommunikation mit Landwirten, Verbänden
* Marie - Teamsupport und Kommunikation mit Landwirten, Verbänden
* Vanessa ? (hat noch nicht zugestimmt)
* Alex Rüffinger - Teamsupport
## 4) Tech (dev-support@ernte-erfolg.de)
[Hier gibt es mehr technische Dokumentation](technischeDoku.md)
### Frontend
* Freddy Wagner - Full-Stack, Java-script, node
* Hans Ferchland - Full-Stack
* Michael Simmelbauer - React, CSS, JavaScript/TypeScript
### Backend
* Fred Hauschel (@naturzukunft) - Java, Spring, Maven, etwas Docker, WebServices, Anbindung von Enterprise Systemen. http://hauschel.de
* Christian Paffhausen - Server, Backend, Domain-Inhaber und Branchenkenner
* Derzeit inaktiv: Chrischu - Datenbanken, SQL, Microsoft
* Kevin Bock - Java, Spring, Backend
### Testing
[Hier gibt es mehr Dokumentation zum Testmanagement](testmanagement.md)
- Monika Ihle - Definition von Testfällen, Durchführung von Tests
## 5) Marketing und Konzeption
### UX/Konzeption
* Tim Sockel - Konzeption, Informatik, Java, React 
* Laura Moll - Konzeption, Frontend-Design, PHP, HTML, CSS, Java
* Alex Rüffer - Konzeption, Design, UX
### Grafik und Video
* Sanni - Grafikdesign, Logo, UI, Fonts
* Marie Ströbe - Grafikdesign, Logo, Video
* Lars - Videoplanung und -schnitt, Sprecher
### Marketing und Research
* Arlette Beck - Konzeption, Orga
* Vanessa Götzinger - Marke, Texte, Instagram
### User Support/Customer Journey
* Myriam Rapior
* Maxim Hasse
* Felix Hartmann
