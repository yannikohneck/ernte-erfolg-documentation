# API Design

## Allgemein
* Login
* E-Mail activation
* Change Password Flow
* Register

## User
* **email**
* **password**
* **activated**
* **type**: "FARMER"|"HELPER"
* **profileId**

## Farm
* **name**
* **lat**
* **lon**
* **street**
* **houseNumber**
* **zipCode**
* **city**
* **phone**
* **contactEmail**: This is redundant to the `email` attribute in the User entity. But maybe a farmer wants to use his personal email for login and a business email for contact with the helpers? In this case, we should leave this attribute in here
* **additionalInformation**

## Helper
* **email** -> from User entity
* **forename**
* **surname**
* **lat**
* **lon**
* **street**
* **houseNumber**
* **zipCode**
* **city**
* **phone**

## Task
* **farmId**
* **farmName**
* **title**
* **requestedDays**: [ `workday`, `workday`, ... ]
* **guaranteedDays**: [ { `workday` }, { `workday` }, ... ]
* **helpers**: *only if requested by "FARMER"* [ { helper }, { helper }, ... ]
* **guaranteedDaysByHelper**: *only if requested by "FARMER"* structure: see `guaranteedDaysByHelper`
* **lat**
* **lon**
* **street**
* **houseNumber**
* **zipCode**
* **city**
* **description**
* **additionalInformation**: This attribute is for additional information about the place, so the helpers can find it more easily (e. g. directions to the field)

## Offer
* **taskId**
* **farmId**
* **farmName**
* **title**
* **requestedDays**: [ `workday`, `workday`, ... ]
* **guaranteedDays**: [ { `workday` }, { `workday` }, ... ]
* **myGuaranteedDays**: [ workday, workday, ... ]

# Structures
```js
const workday = { day: 21, month: 3, year: 2020, workingHours: 3 }
const guaranteedDaysByHelper = {
    helperId1: [ workday, workday, ... ],
    helperId2: [ workday, workday, ... ]
}
```

